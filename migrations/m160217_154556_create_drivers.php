<?php

use yii\db\Migration;

class m160217_154556_create_drivers extends Migration
{
    public function up()
    {
        $this->createTable('drivers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'surname' => $this->string(50)->notNull(),
            'phone' => $this->integer(11)->notNull(),
            'age' => $this->integer()->notNull(),
            'active' => $this->integer(1)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('drivers');
    }
}
