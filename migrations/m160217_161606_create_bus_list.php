<?php

use yii\db\Migration;

class m160217_161606_create_bus_list extends Migration
{
    public function up()
    {
        $this->createTable('bus_list', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('bus_list');
    }
}
