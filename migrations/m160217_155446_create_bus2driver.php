<?php

use yii\db\Migration;

class m160217_155446_create_bus2driver extends Migration
{
    public function up()
    {
        $this->createTable('bus2driver', [
            'id' => $this->primaryKey(),
            'id_bus' => $this->integer()->notNull(),
            'id_driver' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('bus2driver');
    }
}
