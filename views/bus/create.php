<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bus */

$this->title = 'Новая модель';
$this->params['breadcrumbs'][] = ['label' => 'Модели автобусов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
