<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-4 col-sm-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-4 col-sm-6">
        <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-2 col-sm-6">
        <?= $form->field($model, 'phone')->textInput() ?>
    </div>
    <div class="col-lg-2 col-sm-6">
        <?= $form->field($model, 'age')->textInput() ?>
    </div>
    <div class="col-lg-12 col-sm-12">
        <?=
        $form->field($model, 'models')->widget(Select2::classname(), [
            'data' => $bus_list,
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите модели'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
        ])
        ?>
    </div>
    <div class="col-lg-12 col-sm-12">
        <?= $form->field($model, 'active')->checkbox() ?>
    </div>
    <div class="form-group col-lg-12 col-sm-12">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
