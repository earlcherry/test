<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DriverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerJsFile('/js/updateActive.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = 'Водители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый водитель', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'name',
            'surname',
            'phone',
            'age',
            [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'active',
                'format' => 'raw',
                'filter' => false,
                'value' => function ($model, $index, $widget) {
                    return Html::checkbox("active[]", $model->active, ['class'=>'active','value' => $model->active,'data'=>['url'=>Url::to(['driver/active','id'=>$index]),'id'=>$index]]);
                },
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>

</div>
