<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "drivers".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property integer $phone
 * @property integer $age
 * @property integer $active
 */
class Driver extends \yii\db\ActiveRecord {

    public $models = [];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'drivers';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'surname', 'phone', 'age', 'active'], 'required'],
            [['phone', 'age', 'active'], 'integer'],
            [['name', 'surname'], 'string', 'max' => 50],
            [['models'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'age' => 'Возраст',
            'active' => 'Активен',
            'models' => 'Модели автобусов'
        ];
    }

    public function getModels() {
        $this->models = ArrayHelper::getColumn($this->hasMany(Bus2driver::className(), ['id_driver' => 'id'])->all(), 'id_bus');
    }

    public function afterSave($insert, $changedAttributes) {
        Bus2driver::deleteAll(['id_driver' => $this->id]);
        if ($this->models) {
            $values = [];
            foreach ($this->models as $id) {
                $values[] = [$this->id, $id];
            }
            self::getDb()->createCommand()
                    ->batchInsert(Bus2driver::tableName(), ['id_driver', 'id_bus'], $values)->execute();
        }

        parent::afterSave($insert, $changedAttributes);
    }

}
