<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bus2driver".
 *
 * @property integer $id
 * @property integer $id_bus
 * @property integer $id_driver
 */
class Bus2driver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bus2driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_bus', 'id_driver'], 'required'],
            [['id_bus', 'id_driver'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_bus' => 'Id Bus',
            'id_driver' => 'Id Driver',
        ];
    }
}
